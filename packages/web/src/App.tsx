import React from "react";
import { Button } from "@nine/design-system";

function App() {
  return <Button>Hola!</Button>;
}

export default App;
