import * as React from "react";
import styled from "styled-components";

interface ButtonProps {
  disabled?: boolean;
  onClick?: () => void;
  icon?: React.ReactElement;
}

const ButtonStyle = styled.button`
  color: white;
  background-color: blue;
  font-size: 12px;

  &:disabled {
    background-color: grey;
  }
`;

export const Button: React.FC<ButtonProps> = ({
  children,
  icon,
  onClick,
  disabled,
}) => {
  return (
    <ButtonStyle onClick={onClick} disabled={disabled}>
      {icon ? (
        <>
          {icon} {children}
        </>
      ) : (
        children
      )}
    </ButtonStyle>
  );
};
