import * as React from "react";
import ReactModal, { Props } from "react-modal";

export type ModalProps = Pick<
  Props,
  "children" | "isOpen" | "onAfterClose" | "onRequestClose" | "className"
>;

const customStyles = {
  content: {
    top: "50%",
    left: "50%",
    right: "auto",
    bottom: "auto",
    marginRight: "-50%",
    transform: "translate(-50%, -50%)",
  },
};

export const Modal: React.FC<ModalProps> = (props) => {
  return <ReactModal {...props} style={customStyles} />;
};
