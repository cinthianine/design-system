import React from "react";
import { Modal } from "./modal";
import { Button } from "./button";

export default {
  title: "Modal",
  component: Modal,
};

export const modalComponent = () => {
  const [isOpen, setIsOpen] = React.useState(false);

  const openModal = () => {
    setIsOpen(true);
  };

  const closeModal = () => {
    setIsOpen(false);
  };

  return (
    <>
      <Button onClick={openModal}>Open modal</Button>

      <Modal isOpen={isOpen} onRequestClose={closeModal}>
        <h2>Hello</h2>
        <p>This is Nine modal</p>

        <Button onClick={closeModal}>Close</Button>
      </Modal>
    </>
  );
};
