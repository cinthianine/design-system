import React from "react";
import { Button } from "./button";

export default {
  title: "Button",
  component: Button,
};

export const ButtonComponent = () => <Button>button</Button>;

export const ButtonComponentDisabled = () => (
  <Button disabled={true}>button</Button>
);

export const ButtonComponentWithIcon = () => (
  <Button icon={<>🤓</>}>button</Button>
);
